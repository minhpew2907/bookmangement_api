using Microsoft.AspNetCore.Mvc;

List<Book> books = new List<Book>();

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

WebApplication app = builder.Build();

if (app.Environment.IsDevelopment())
    app.MapGet("/books/all", () =>
    {
        app.UseSwagger();
        app.UseSwaggerUI();
        return Results.Ok(books);
    });

app.MapPost("/books", ([FromBody] Book book) =>
{
    book.Id = book.Id;
    books.Add(book);
    return Results.Ok(books);
});

app.MapGet("/books/GetById/{id}", (int id) =>
{
    var book = books.FirstOrDefault(x => x.Id == id);
    return Results.Ok(book);
});

app.MapPut("/books/Update/{id}", (int id, [FromBody] Book updateBook) =>
{
    var book = books.FirstOrDefault(x => x.Id == id);
    book.Title = updateBook.Title;  
    book.Author = updateBook.Author;
    return book;
});

app.MapDelete("/books/Delete/{id}", (int id) =>
{
    var book = books.FirstOrDefault(x => x.Id == id);
    books.Remove(book);
    return book;
});

app.MapControllers();
app.UseHttpsRedirection();
app.Run();

public class Book
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }
}